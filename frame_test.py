"""
μ3d Frame frame_test.py
frame/frame_test.py tests the package.
"""
from frame import World, Data, Physics, Model
from rp import RPShowBase

data = Data('./frame-data.toml')

world = World(
  showbase = RPShowBase,
  data = data,
  physics = Physics(),
  backdrop = Model(data('$.assets.backdrop'), 'backdrop'),
  box = Model(data('$.assets.box'), 'box'),
)

#                         | later |
#           | task id  |  |
@world.task('cam-rotate', 0)
def cam_rotate(world, task):
  mouse = world.base.mouseWatcherNode
  cam = world.base.cam
  if mouse.hasMouse():
    x = mouse.getMouseX()
    y = mouse.getMouseY()
    cam.set_h(x * world.__data('$.logic.mouse.h_mul'))
    cam.set_p(y * world.__data('$.logic.mouse.p_mul'))
  return task.cont

@world.at('arrow_up-down')
def key_w(world, event):
  cam = world.base.cam
  pos = cam.get_pos()
  pos[0] += 1
  cam.set_pos(*pos)

@world.at('arrow_left-down')
def key_w(world, event):
  cam = world.base.cam
  pos = cam.get_pos()
  pos[1] += 1
  cam.set_pos(*pos)

@world.at('arrow_right-down')
def key_w(world, event):
  cam = world.base.cam
  pos = cam.get_pos()
  pos[1] -= 1
  cam.set_pos(*pos)

@world.at('arrow_down-down')
def key_w(world, event):
  cam = world.base.cam
  pos = cam.get_pos()
  pos[0] -= 1
  cam.set_pos(*pos)

# scene = world.base.loader.loadModel("models/environment")
# scene.reparentTo(world.base.render)
# scene.setScale(0.25, 0.25, 0.25)
# scene.setPos(-8, 42, 0)

if __name__ == '__main__':
  world.base.run()
