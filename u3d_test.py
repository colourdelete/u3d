import u3d

app = u3d.App('μ3D Test')
app.config = u3d.Config('./u3d_test.toml')
app.physics = u3d.Physics(app['$.sys.config.logic.physics'])

if __name__ == '__main__':
  app()
