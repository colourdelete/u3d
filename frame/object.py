"""
μ3d Frame object.py
"""

class Object:
  def __init__(self, **children):
    self.children = children
  
  def __getattr__(self, item: str):
    if item.startswith('_'):
      return self.children[item]
    else:
      raise AttributeError(f'Object has no attribute \'{item}\'')
