"""
μ3d Frame __data.py
"""
import json
import os

import toml
import xmltodict
import jsonpath_ng

from .object import Object


class Data(Object):
  """
  Data reads, writes, and stores __data in TOML, JSON, or XML.
  """
  def __init__(self, path: str, data: dict = None):
    super().__init__()
    self.data = data
    if path != "":
      self.path = path
      self.file_ext = os.path.splitext(path)[1].lower()
      if self.file_ext == '.toml':
        with open(path) as file:
          self.data = toml.load(file)
      elif self.file_ext == '.json':
        with open(path) as file:
          self.data = json.load(file)
      elif self.file_ext == '.xml':
        with open(path) as file:
          self.data = xmltodict.parse(file.read())
      else:
        raise TypeError(f'path must have extension \'.toml\', \'.json\', or \'.xml\'; got \'{self.file_ext}\'')
  
  def __call__(self, path: str) -> object:
    expr = jsonpath_ng.parse(path)
    matches = expr.find(self.data)
    if len(matches) == 0:
      raise NameError(path)
    else:
      return matches[0].value
  
  def write(self, path: str = None):
    """
    Data.write writes Data.__data onto path or (if path is None) Data.path.
    :param path: path to save to. if None, will default to Data.path.
    """
    if path is None:
      path = self.path
    if self.file_ext == 'toml':
      with open(path, 'w') as file:
        toml.dump(self.data, file)
    elif self.file_ext == 'json':
      with open(path, 'w') as file:
        json.dump(self.data, file)
    elif self.file_ext == 'xml':
      with open(path, 'w') as file:
        file.write(xmltodict.unparse(self.data))
    else:
      raise TypeError('path must have extension \'toml\', \'json\', or \'xml\'')
