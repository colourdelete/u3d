"""
μ3d Frame __init__.py
"""
from .data import *
from .object import *
from .physics import *
from .world import *
from .actor import *
