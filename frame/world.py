"""
μ3d Frame world.py
"""
from direct.showbase.ShowBase import ShowBase

from .actor import Model
from .object import Object
from .task import Task


class World(Object):
  """
  World is a Panda3D ShowBase wrapper.
  """
  def __init__(self, physics = None, data = None, showbase = ShowBase, showbase_args = [], showbase_kwargs = {}, **children):
    super().__init__(**children)
    self.base = showbase(*showbase_args, **showbase_kwargs)
    self.physics = physics
    self.data = data
    for _, child in self.children.items():
      if isinstance(child, Model):
        child.apply(self)

  def task(self, name: str, later, **kwargs):
    """
    World.task is a decorator generator for defining tasks.
    :param name: the name of the task
    :return: decorator
    """
    def decorator(wrapped):
      task = Task(wrapped, self, name)
      if later == 0:
        task.add(**kwargs)
      else:
        task.later(later, **kwargs)
      return task
    return decorator

  def at(self, event: str, args: list = [], kwargs: dict = {}):
    """
    World.at is a decorator generator for defining callbacks to events.
    :param event: the message to receive
    :return: decorator
    """
    def decorator(wrapped):
      def wrapper(*args_, **kwargs_):
        return wrapped(self, *args, *args_, **kwargs, **kwargs_)
      self.base.accept(event, wrapper)
      return wrapped
    return decorator

  def at_once(self, event: str, args: list):
    """
    World.at_once is a decorator generator for defining callbacks to events that only gets called once.
    :param event: the message to receive
    :return: decorator
    """
    def decorator(wrapped):
      self.base.accept_once(event, wrapped, extraArgs = args)
      return
    return decorator
