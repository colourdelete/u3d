import json
import os

import strictyaml
import toml
import xmltodict
import yaml


class Config(dict):
  def __init__(self, path: str, data: dict = None):
    super().__init__()
    if not isinstance(path, str):
      raise TypeError(f'path must be str, not {type(path)}')
    if not isinstance(data, dict) and data is not None:
      raise TypeError(f'data must be dict, not {type(data)}')
    if path == '' or path is None:
      self.path = None
    else:
      self.path = path
    if data is None:
      self.load()
    else:
      self.data = data
  
  def __getitem__(self, item: str):
    return self.data[item]
  
  def __setitem__(self, key: str, value: object):
    self.data[key] = value

  def load(self):
    path_ext = os.path.splitext(self.path)[1]
    if path_ext == '.toml':
      with open(self.path) as file:
        self.data = toml.load(file)
    elif path_ext == '.json':
      with open(self.path) as file:
        self.data = json.load(file)
    elif path_ext == '.yaml':
      with open(self.path) as file:
        self.data = strictyaml.load(file.read())
    elif path_ext == '.xml':
      with open(self.path) as file:
        self.data = xmltodict.parse(file.read())
    else:
      raise TypeError(f'self.path extension must be .toml, .json, .yaml, or .xml, not {path_ext}')

  def dump(self):
    path_ext = os.path.splitext(self.path)[1]
    if path_ext == '.toml':
      with open(self.path, 'w') as file:
        toml.dump(self.data, file)
    elif path_ext == '.json':
      with open(self.path, 'w') as file:
        json.dump(self.data, file)
    elif path_ext == '.yaml':
      with open(self.path, 'w') as file:
        yaml.dump(self.data, file)
    elif path_ext == '.xml':
      with open(self.path, 'w') as file:
        file.write(xmltodict.unparse(self.data))
    else:
      raise TypeError(f'self.path extension must be .toml, .json, .yaml, or .xml, not {path_ext}')
