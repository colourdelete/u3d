from direct.showbase.ShowBaseGlobal import globalClock
from panda3d.bullet import BulletWorld


class Physics:
  def __init__(self, config: dict):
    if not isinstance(config, dict):
      raise TypeError(f'config must be dict, not {type(config)}')
    self.config = config
    self.world = None
  
  def apply(self, app):
    self.world = BulletWorld()
    
    @app.task('$.tasks.sys.__physics')
    def task_sys_physics(_, task):
      dt = globalClock.getDt()
      self.world.doPhysics(dt)
      return task.cont
