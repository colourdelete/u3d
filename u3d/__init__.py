import sys

if sys.version_info[0] != 3:
  raise RuntimeError(f'Python version must be 3, not {sys.version_info[0]}')

from .app import *
from .config import *
